#include <cstdio>
#include <cassert>

long long int numero_mosse(int n) {
    /*
        Ritorna il numero di mosse per spostare
        una torre di n dischi da un piolo
        a un altro.
    */
    assert(n >= 1);
    if(n == 1) return 1;
    return 2*numero_mosse(n-1) + 1;
}

void sposta_disco(int n, char a, char b) {
    printf("Sposta il disco %d da %c a %c\n",
        n, a, b);
}

void stampa_mosse(int n, char a, char b, char c) {
    /* Stampa le mosse per spostare
        una torre di n dischi dal piolo 'a'
        al piolo 'b'
        usando 'c' d'appoggio. */
    assert(n >= 0);
    if(n == 0) return;

    stampa_mosse(n-1, a, c, b);
    sposta_disco(n, a, b);
    stampa_mosse(n-1, c, b, a);
}

int main() {
    int n;
    scanf("%d", &n);
    printf("Numero mosse: %lld\n", numero_mosse(n));
    stampa_mosse(n, 'A', 'B', 'C');
}
