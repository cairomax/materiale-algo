#include <cstdio>
#include <cassert>
#include <vector>

using std::vector;

int prossimo_nodo = 0;
// figli[u] = sequenza ordinata degli ID dei figli del nodo u
vector<int> figli[10000];

void leggi_albero() {
    // legge un intero sottoalbero radicato in 'prossimo_nodo'
    // e incrementa 'prossimo_nodo' al piu' piccolo
    // ID ancora non usato
    int u = prossimo_nodo;
    prossimo_nodo++;

    int n_figli;
    scanf("%d", &n_figli);
    figli[u].resize(n_figli);

    for(int i = 0; i < n_figli; i++) {
        figli[u][i] = prossimo_nodo;
        leggi_albero();
    }
}

void stampa_albero(int radice) {
    // stampa il sottoalbero radicato in 'radice'
    int n_figli = figli[radice].size();
    printf("%d ", n_figli);
    for(int i = n_figli-1; i >= 0; i--) {
        int figlio = figli[radice][i];
        stampa_albero(figlio);
    }
}

int main() {
    leggi_albero();

    if(false) {
        for(int u = 0; u < prossimo_nodo; u++) {
            printf("Figli di %2d:", u);
            for(int i = 0; i < figli[u].size(); i++)
                printf(" %d", figli[u][i]);
            printf("\n");
        }
    }
    
    stampa_albero(0);
    printf("\n");
}
