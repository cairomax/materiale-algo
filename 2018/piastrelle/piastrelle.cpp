#include <cstdio>
#include <cassert>

bool nota[100000];
long long memo[100000];

long long num_piastrellature(int n) {
    // ritorna numero di piastrellature
    // in un bagno di dimensione n

    printf("num_piastrellature(%d)...\n", n);

    if(nota[n]) {
        printf("nota!\n", n);
        return memo[n];
    }

    assert(n >= 0);
    if(n == 0 || n == 1) {
        return 1;
    }

    memo[n] = num_piastrellature(n-1) + num_piastrellature(n-2);
    nota[n] = true;
    return memo[n];
}

int main() {
    int n;
    scanf("%d", &n);
    printf(
        "num_piastrellature(%d) -> %lld\n",
        n, num_piastrellature(n)
    );
}
