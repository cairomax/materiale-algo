#include <cstdio>
#include <cassert>
#include <vector>

using std::vector;

const int N_MAX = 400000;

int N;
int W[N_MAX + 1];

vector<int> A[N_MAX + 1];

const int root = 1;
int parent[N_MAX + 1];

/*
    con[u] := costo minimo soluzione sottoalbero radicato in u
    vincolato a mettere una telecamera in u.
*/
int con[N_MAX + 1];

/*
    senza[u] := costo minimo soluzione sottoalbero radicato in u
    vincolato a NON mettere una telecamera in u.
*/
int senza[N_MAX + 1];

/*
    con[u] := costo minimo soluzione sottoalbero radicato in u.
*/
int libero[N_MAX + 1];

// determina il padre di ogni nodo (parent[u])
void dfs(int u) {
    for(int i = 0; i < A[u].size(); i++) {
        int v = A[u][i];
        if(v == parent[u]) continue;

        parent[v] = u;
        dfs(v);
    }
}

// contratto: calcola con[u], senza[u] e libero[u]
void dfs2(int u) {
    for(int i = 0; i < A[u].size(); i++) {
        int v = A[u][i];
        if(v == parent[u]) continue;

        dfs2(v);
    }
    
    con[u] = W[u];
    for(int i = 0; i < A[u].size(); i++) {
        int v = A[u][i];
        if(v == parent[u]) continue;
        con[u] += libero[v];
    }
    
    senza[u] = 0;
    for(int i = 0; i < A[u].size(); i++) {
        int v = A[u][i];
        if(v == parent[u]) continue;
        senza[u] += con[v];
    }
    
    if(con[u] < senza[u]) {
        libero[u] = con[u];
    } else {
        libero[u] = senza[u];
    }
}

bool prendo[N_MAX + 1];
vector<int> presi;

// contratto: 
void dfs3(int u, bool obbligo) {
    prendo[u] = obbligo || (con[u] <= senza[u]);

    for(int i = 0; i < A[u].size(); i++) {
        int v = A[u][i];
        if(v == parent[u]) continue;

        dfs3(v, !prendo[u]);
    }
    
    if(prendo[u]) {
        presi.push_back(u);
    }
}

int main() {
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    scanf("%d", &N);
    assert(2 <= N && N <= N_MAX);

    for(int u = 1; u <= N; u++) {
        scanf("%d", &W[u]);
    }
    
    for(int i = 0; i < N-1; i++) {
        int u, v;
        scanf("%d%d", &u, &v);
        
        A[u].push_back(v);
        A[v].push_back(u);
    }
    
    for(int u = 1; u <= N; u++) {
        //printf("Il nodo %d ha %d vicini:", u, A[u].size());
        for(int i = 0; i < A[u].size(); i++) {
            int v = A[u][i];
            //printf(" %d", v);
        }
        //printf("\n");
    }

    parent[root] = -1;
    dfs(root);

    for(int u = 1; u <= N; u++) {
        //printf("Il nodo %d ha padre %d.\n", u, parent[u]);
    }
    
    dfs2(root);
    
    for(int u = 1; u <= N; u++) {
        //printf("con[%d] = %d;   senza[%d] = %d;   libero[%d] = %d;\n",
        //        u, con[u], u, senza[u], u, libero[u]);
    }
    
    dfs3(root, false);
    
    printf("%d\n", presi.size());
    for(int u = 1; u <= N; u++) {
        if(prendo[u]) {
            printf("%d ", u);
        }
    }
    printf("\n");
        
    return 0;
}
