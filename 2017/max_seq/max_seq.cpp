#include <assert.h>
#include <stdio.h>

int main() {
    FILE* fi = fopen("input.txt", "r");
    assert(fi);
    
    int N;
    fscanf(fi, "%d", &N);
    
    int max_so_far;
    fscanf(fi, "%d", &max_so_far);
    
    for(int i = 1; i < N; i++) {
        int k;
        fscanf(fi, "%d", &k);
        
        if(k > max_so_far) {
            max_so_far = k;
        }
    }
    
    FILE* fo = fopen("output.txt", "w");
    fprintf(fo, "%d\n", N);
    fprintf(fo, "%d\n", max_so_far);
    
    return 0;
}

