#include <cstdio>
#include <cassert>

unsigned long long int num_mosse(int N) {
    // ritorno numero di mosse per spostare una torre di N dischi
    assert(N >= 0);
    if(N == 0) return 0;
    return 1 + 2 * num_mosse(N-1);
}

void sposta_disco(int N, int from, int to) {
    printf("Sposto il disco %d dal piolo %c al piolo %c\n",
            N, from, to);
}

void sposta_torre(int N, int from, int to, int piolo_di_appoggio) {
    // sposta una torre di N dischi (da 1 a N)
    // incolonnati nel piolo from
    // portandoli incolonnati nel piolo to
    assert(N >= 0);
    if(N == 0) return;
    
    assert(N >= 1);
    
    sposta_torre(N-1, from, piolo_di_appoggio, to);
    sposta_disco(N, from, to);
    sposta_torre(N-1, piolo_di_appoggio, to, from);
}

int main() {
    int N;
    
    scanf("%d", &N);
    
    unsigned long long int mosse = num_mosse(N);
    printf("Mosse: %llu\n", mosse);

    sposta_torre(N, 'A', 'C', 'B');

    return 0;
}

