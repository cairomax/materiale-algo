#include <cstdio>
#include <cassert>
#include <cstring>

const int LEN_MAX = 1000;

int N, M; // lunghezza di S e di T
char S[LEN_MAX + 2], T[LEN_MAX + 2]; // indicizzate con 1..N e 1..M

void allinea_singolo(int index_S, int start_T, int end_T) {
    assert(1 <= index_S && index_S <= N);
    assert(1 <= start_T && start_T <= end_T+1 && end_T <= M);

    if(start_T == end_T+1) {
        printf("%c _ (cancellazione %2d)\n", S[index_S], index_S);
        return;
    }

    if(S[index_S] == T[start_T]) {
        printf("%c %c (allinea %2d-%2d)\n",
                S[index_S], T[start_T], index_S, start_T);
        for(int i = start_T+1; i <= end_T; i++) {
            printf("_ %c (inserisci %2d)\n", T[i], i);
        }
        return;
    }

    if(start_T == end_T) {
        printf("%c %c (modifica %2d-%2d)\n",
                S[index_S], T[start_T], index_S, start_T);
        return;
    }

    printf("_ %c (inserisci %2d)\n", T[start_T], start_T);
    allinea_singolo(index_S, start_T+1, end_T);
}

int L[LEN_MAX+2][LEN_MAX+2];
int R[LEN_MAX+2][LEN_MAX+2];

int magic_break(int start_S, int mid_S, int end_S, int start_T, int end_T) {
    // restituisce indice mid_T
    // tale che il problema S[start_S..end_S] in T[start_T..end_T]
    // si spezza in
    // S[start_S..mid_S], T[start_T..mid_T] e
    // S[mid_S+1..end_S], T[mid_T+1..mid_T]
    
    /*
        L[i][j] := edit_distance(
            S[start_S..i],
            T[start_T..j]
        )

        R[i][j] := edit_distance(
            S[i..end_S],
            T[start_T..j]
        )
    */

    for(int j = start_T-1, j <= end_T; j++) {
        L[start_S-1][j] = j-start_T+1;
        R[end_S  +1][j] = j-start_T+1;
    }
    
    for(int i = start_S; i <= mid_S; i++) {
        for(int j = start_T, j <= end_T; j++) {
            // TODO: finire programmazione dinamica
        }
    }
    
    return start_T;
}

void genera_alignment(int start_S, int end_S, int start_T, int end_T)
{
    // stampa lista mosse per trasformare
    // S[start_S..end_S] in T[start_T..end_T]
    assert(1 <= start_S && start_S <= end_S   && end_S <= N);
    assert(1 <= start_T && start_T <= end_T+1 && end_T <= M);
    
    if (start_S == end_S) {
        allinea_singolo(start_S, start_T, end_T);
        return;
    }
    
    int mid_S = (start_S + end_S) / 2;
    int mid_T = magic_break(
            start_S, mid_S, end_S, start_T, end_T);
    
    genera_alignment(start_S, mid_S, start_T, mid_T);
    genera_alignment(mid_S+1, end_S, mid_T+1, end_T);
}

int main() {
    scanf("%d %s", &N, &S[1]);
    assert(strlen(&S[1]) == N);
    fprintf(stderr, "Ho letto S: '%s'\n", &S[1]);
    
    scanf("%d %s", &M, &T[1]);
    assert(strlen(&T[1]) == M);
    fprintf(stderr, "Ho letto T: '%s'\n", &T[1]);
    
    genera_alignment(1, N, 1, M);
}

