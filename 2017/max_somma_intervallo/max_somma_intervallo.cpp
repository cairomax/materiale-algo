#include <stdio.h>
#include <assert.h>

int main() {
    FILE *fi = fopen("input.txt", "r");
    assert(fi);
    
    int N;
    fscanf(fi, "%d", &N);
    
    int array[N];
    for(int i = 0; i < N; i++)
        fscanf(fi, "%d", &array[i]);
    
    int best_so_far = array[0];
    for(int i = 0; i < N; i++) {
        for(int j = i; j < N; j++) {
            int somma = 0;
            for(int k = i; k <= j; k++)
                somma += array[k];
            if(somma > best_so_far)
                best_so_far = somma;
        }
    }
    
    FILE *fo = fopen("output.txt", "w");
    assert(fo);
    fprintf(fo, "%d\n", best_so_far);
    return 0;
}
