#include <stdio.h>
#include <assert.h>

const int MAX_N = 1000000;

int array[MAX_N];
// P[i] = massima somma in un intervallo che inizia da posizione i
int P[MAX_N];

int main() {
    FILE *fi = fopen("input.txt", "r");
    assert(fi);
    
    int N;
    fscanf(fi, "%d", &N);
    
    for(int i = 0; i < N; i++)
        fscanf(fi, "%d", &array[i]);

    P[N-1] = array[N-1];
    for(int i = N-2; i >= 0; i--) {
        P[i] = array[i];
        if(P[i+1] > 0) {
            P[i] += P[i+1];
        }
    }
    int soluzione = P[0];
    for(int i = 0; i < N; i++) {
        if(P[i] > soluzione) {
            soluzione = P[i];
        }
    }
    
    FILE *fo = fopen("output.txt", "w");
    assert(fo);
    fprintf(fo, "%d\n", soluzione);

    return 0;
}
