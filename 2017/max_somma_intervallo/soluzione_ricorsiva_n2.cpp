#include <stdio.h>
#include <assert.h>

const int MAX_N = 1000;

int array[MAX_N];

int max_value_ric(int n) {
    // Restituisci la massima somma di
    // un sotto-intervallo di array[0..n-1]
    
    // BASE
    assert(n >= 1);
    if(n == 1) {
        return array[0];
    }
    
    // PASSO INDUTTIVO
    int max_so_far = max_value_ric(n-1);
    
    int somma_so_far = 0;
    for(int left = n-1; left >= 0; left--) {
        somma_so_far += array[left];
        // INVARIANTE: somma_so_far = somma di array[left..n-1]
        
        if(somma_so_far > max_so_far) {
            max_so_far = somma_so_far;
        }
        // INVARIANTE: max_so_far = massima somma fra tutti
        // i sotto-intervalli di array[0..n-2] o array[left..n-1]
    }
    
    return max_so_far;
}

int main() {
    FILE *fi = fopen("input.txt", "r");
    assert(fi);
    
    int N;
    fscanf(fi, "%d", &N);
    
    for(int i = 0; i < N; i++)
        fscanf(fi, "%d", &array[i]);

    int soluzione = max_value_ric(N);
    
    FILE *fo = fopen("output.txt", "w");
    assert(fo);
    fprintf(fo, "%d\n", soluzione);

    return 0;
}
