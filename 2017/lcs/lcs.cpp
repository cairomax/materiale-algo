#include <cstdio>
#include <cassert>
#include <cstring>

const int LEN_MAX = 1000;

int N, M; // lunghezza di S e di T
char S[LEN_MAX + 2], T[LEN_MAX + 2]; // indicizzate con 1..N e 1..M

int my_max(int a, int b) {
    if(a > b) return a;
    return b;
}

int len_max_common_subseq(int i, int j) {
    /*
    Ritorna la lunghezza della massima sottosequenza comune
    fra S[i..N] e T[j..M]
    */
    assert(1 <= i && i <= N+1);
    assert(1 <= j && j <= M+1);
    
    if(i > N || j > M) return 0;
    if(S[i] == T[j]) return 1 + len_max_common_subseq(i+1, j+1);
    
    return my_max(
        len_max_common_subseq(i+1, j  ),
        len_max_common_subseq(i  , j+1)
    );
}

int P[LEN_MAX+1][LEN_MAX+1];

int main() {
    scanf("%d %s", &N, &S[1]);
    scanf("%d %s", &M, &T[1]);
    
    assert(strlen(&S[1]) == N);
    assert(strlen(&T[1]) == M);
    
    // printf("%d\n", len_max_common_subseq(1,1));
    
    for(int i = N; i >= 0; i--) {
        for(int j = M; j >= 0; j--) {
            if(S[i] == T[j]) P[i][j] = P[i+1][j+1];
            else P[i][j] = my_max(
                P[i+1][j  ],
                P[i  ][j+1]
            )
        }
    }
}

